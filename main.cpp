#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#include "myhash.h"

#include <string>
#include <iostream>
#include <vector>

#define MSG_SIZE 10
#define PASSIVE_SOCK_ADR "/home/strong/Git/puppeteer-shell/current_sock"

#define MAINJS "/home/strong/Git/puppeteer-the-photographer/main.js"
#define DBPHOTO_TMP "/home/strong/DBPHOTO_TMP/"

void run_puppeteer(std::string url, std::string ip);

int main()
{
  sockaddr_un addr;
  std::string url, response_msg;
  uint32_t ip;
  int sock, fid;
  char read_buf[MSG_SIZE];
  uint16_t urlsz;

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, PASSIVE_SOCK_ADR, sizeof(addr.sun_path));

  while (1) {
    printf("\nEnter url and ip : ");
    std::cin >> url >> ip;
    std::cout << std::endl;

    urlsz = url.size();

    fid = fork();
    if (fid == 0) {
      run_puppeteer(url, std::to_string(ip));
      return 0;
    }
    else {
      waitpid(fid, NULL, 0);

      if ((sock = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket error");
        continue;
      }

      if (connect(sock, (sockaddr*)&addr, sizeof(addr)) == -1) {
        perror("connect error");
        continue;
      }

      if (write(sock, &ip, sizeof(ip)) == -1) {
        perror("write error");
        close(sock);
        continue;
      }

      if (write(sock, &urlsz, sizeof(urlsz)) == -1) {
        perror("write error");
        close(sock);
        continue;
      }

      if (write(sock, url.data(), urlsz) == -1) {
        perror("write error");
        close(sock);
        continue;
      }

      printf("URL and Ip sent to puppeteer-shell\n");

      close(sock);
    }
  }
  return 0;
}

void run_puppeteer(std::string url, std::string ip)
{
  std::string hashed_url, command;
  std::vector<std::string> args;

  command.clear(), args.clear();
  hashed_url = myhash((char *) url.data());

  args.push_back("node");
  args.push_back(MAINJS);
  args.push_back(url);
  args.push_back((std::string) DBPHOTO_TMP + hashed_url);
  args.push_back("8811");
  args.push_back(ip);

  for (int i = 0; i < (int) args.size(); i++)
    command += args[i] + " ";

  printf("Running puppeteer with proxy set to : cache-creator (8811): %s\n", command.data());
  system(command.data());
}
