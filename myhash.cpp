#include "myhash.h"

std::string myhash(char *data)
{
  size_t length = strlen(data);
  unsigned char hash[SHA_DIGEST_LENGTH];
  std::string hash_hex;

  hash_hex.clear();

  SHA1((const unsigned char *) data, length, hash);
  to_hex(hash, &hash_hex);

  return hash_hex;
}

void to_hex(unsigned char *hash, std::string *hash_hex)
{
  char ch[3];
  unsigned char tmp;

  for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
    memset(ch, 0, 3);
    tmp = hash[i];
    sprintf(ch, "%x", tmp);
    (*hash_hex) += ch;
  }
}
